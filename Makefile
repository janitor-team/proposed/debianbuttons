#!/usr/bin/make -f
# vim: noet sts=8 sw=8 ts=8 ft=makefile

PKG := icedeb

ALL_FILES := README TODO Changes manifest.json \
    icedeb.js icedeb.css icedeb.html icedeb-content.js \
    $(shell find res -type f) \
    icons/openlogo-nd.svg \
    icons/icedeb-addon-light.svg icons/icedeb-addon-dark.svg

all: $(PKG).xpi
$(PKG).xpi: $(ALL_FILES)
	rm -f $@
	zip $@ $^
